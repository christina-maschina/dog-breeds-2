package hr.tvz.android.fragmentividakovic;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import java.util.ArrayList;
import java.util.List;

public class ShareDialog extends DialogFragment {
    private float userRating;
    private Context context;

    public ShareDialog(Context context, float userRating) {
        this.userRating = userRating;
        this.context = context;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        List<Integer> items = new ArrayList<>();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle("Do you want to share the rating?")
                .setPositiveButton("SHARE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String uniqueActionString = "hr.android.broadcast.share";
                        Intent broadcastIntent = new Intent();
                        broadcastIntent.setAction(uniqueActionString);
                        broadcastIntent.putExtra("rating", userRating);
                        context.sendBroadcast(broadcastIntent);
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });

        return builder.create();
    }
}
