package hr.tvz.android.fragmentividakovic;


import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.messaging.FirebaseMessaging;

import hr.tvz.android.fragmentividakovic.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        MainFragment.OnItemSelectedListener, DetailsFragment.OnCallbackReceived {
    private ActivityMainBinding binding;
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle actionBarDrawerToggle;
    Toolbar toolbar;
    NavigationView navigationView;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    public static String MY_CHANNEL = "myChannel";
    private float userRating;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        toolbar = findViewById(R.id.toolbar);
        drawerLayout = binding.drawer;
        navigationView = binding.navigationView;
        navigationView.setNavigationItemSelectedListener(this);

        setSupportActionBar(toolbar);

        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
        actionBarDrawerToggle.syncState();

        //load default fragment
        if (findViewById(R.id.item_detail_container) == null) {
            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.item_list_container, new MainFragment());
            fragmentTransaction.commit();
        }

        // Kreiranje kanala za notifikacije
        // Provjera da li je API 26+
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(MY_CHANNEL, "Notification channel",
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription("Description");
            NotificationManager notificationManager = (NotificationManager) (getSystemService(Context.NOTIFICATION_SERVICE));
            notificationManager.createNotificationChannel(channel);
        }

        // Dohvat registracijskog tokena uređaja za Firebase
        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (!task.isSuccessful()) {
                            Log.w("Main activity", "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult();

                        Log.d("Main activity", token);
                        //Toast.makeText(MainActivity.this, token, Toast.LENGTH_SHORT).show();
                    }
                });

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        drawerLayout.closeDrawer(GravityCompat.START);
        if (item.getItemId() == R.id.home) {
            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            if (findViewById(R.id.item_detail_container) == null) {
                fragmentTransaction.replace(R.id.item_list_container, new MainFragment());
            }

            fragmentTransaction.commit();
        }
        if (item.getItemId() == R.id.gallery) {
            Intent intent = new Intent(this, GalleryActivity.class);
            startActivity(intent);
        }
        return true;
    }

    @Override
    public void OnItemSelected(DogBreeds dogBreeds) {
        Bundle arguments = new Bundle();
        arguments.putParcelable("dogBreeds", dogBreeds);
        DetailsFragment detailFragment = new DetailsFragment();
        detailFragment.setArguments(arguments);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager()
                .beginTransaction();

        if (findViewById(R.id.item_detail_container) != null) {
            Log.d("TWOPAIN", "twopain = true");

            fragmentTransaction
                    .replace(R.id.item_detail_container, detailFragment)
                    .commit();

        } else {
            Log.d("TWOPAIN", "twopain = false");
            fragmentTransaction
                    .addToBackStack(null)
                    .replace(R.id.item_list_container, detailFragment)
                    .commit();
        }
    }

    @Override
    public void send(float userRating) {
        this.userRating = userRating;
    }
}