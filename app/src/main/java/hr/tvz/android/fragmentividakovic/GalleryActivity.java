package hr.tvz.android.fragmentividakovic;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import hr.tvz.android.fragmentividakovic.databinding.ActivityMainBinding;

public class GalleryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        GridView gridView = findViewById(R.id.gridView);
        gridView.setAdapter(new GridImageAdapter(this));
    }
}
