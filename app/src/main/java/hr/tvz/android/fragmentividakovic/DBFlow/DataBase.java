package hr.tvz.android.fragmentividakovic.DBFlow;

import com.raizlabs.android.dbflow.annotation.Database;

@Database(version = DataBase.VERSION, name = DataBase.NAME)
public class DataBase {
    public static final int VERSION=1;
    public static final String NAME="DogBreeds";
}
