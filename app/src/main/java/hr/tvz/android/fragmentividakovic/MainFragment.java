package hr.tvz.android.fragmentividakovic;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.ArrayList;
import java.util.List;

import hr.tvz.android.fragmentividakovic.DBFlow.DogBreedsDBModel;

public class MainFragment extends Fragment {
    RecyclerView recyclerView;
    MyRecyclerViewAdapter adapter;
    private Context context;
    OnItemSelectedListener listener;
    private List<DogBreedsDBModel> listDBModel= new ArrayList<>();
    List<DogBreeds> list=new ArrayList<>();

    String s1[], s2[];
    int images[] = {R.drawable.labrador_retriever, R.drawable.german_shepherd, R.drawable.golden_retriever,
            R.drawable.bulldog, R.drawable.beagle, R.drawable.french_bulldog, R.drawable.yorkshire_terrier,
            R.drawable.poodle, R.drawable.rottweiler, R.drawable.boxer, R.drawable.german_shorthaired_pointer,
            R.drawable.siberian_husky, R.drawable.dachshund, R.drawable.doberman, R.drawable.great_dane,
            R.drawable.miniature_schnauzer, R.drawable.cavalier_king, R.drawable.shih_tzu, R.drawable.pemborke,
            R.drawable.pomeranian};


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (!(context instanceof OnItemSelectedListener)) {
            throw new ClassCastException(context.toString() + " must implement listener");
        }
        listener = (OnItemSelectedListener) context;
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);
        s1 = getResources().getStringArray(R.array.dogs);
        s2 = getResources().getStringArray(R.array.description);

        createDatabase(s1, s2, images);
        retrieveData();

        adapter = new MyRecyclerViewAdapter(((MainActivity) context).getSupportFragmentManager(),
                  s2, list, images, (MainActivity) context, listener);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_HEADSET_PLUG);
        requireActivity().registerReceiver(new SystemReceiver(), intentFilter);
        return view;
    }

    private void createDatabase(String s1[], String s2[], int images[]) {
        int i = 0;
        for (String s : s1) {
            DogBreedsDBModel dog=new DogBreedsDBModel();
            dog.setName(s1[i]);
            dog.setDescription(s2[i]);
            dog.setImages(images[i]);
            FlowManager.getModelAdapter(DogBreedsDBModel.class).save(dog);
            i++;
        }
    }

    private void retrieveData() {
        listDBModel=SQLite.select().from(DogBreedsDBModel.class).queryList();

        for (DogBreedsDBModel dogModel: listDBModel ){
            list.add(new DogBreeds(dogModel.getName(), dogModel.getDescription(),
                    dogModel.getImages()));
        }
    }

    public interface OnItemSelectedListener {
        void OnItemSelected(DogBreeds dogBreeds);
    }
}
