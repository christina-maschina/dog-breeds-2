package hr.tvz.android.fragmentividakovic;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.raizlabs.android.dbflow.sql.language.Operator;

import java.util.Objects;

public class DetailsFragment extends Fragment {
    private TextView titleView, descriptionView;
    private ImageView imageView;
    private String dataName, dataDescription;
    private int dataImage;
    private RatingBar ratingBar;
    float userRating;
    private Context context;
    OnCallbackReceived mCallback;

    public DetailsFragment() { }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context=context;
        try {
            mCallback=(OnCallbackReceived)context;
        }catch (ClassCastException e){}
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState==null){
            setHasOptionsMenu(true);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_details, container, false);
        titleView = view.findViewById(R.id.title);
        descriptionView = view.findViewById(R.id.description);
        imageView = view.findViewById(R.id.img);
        ratingBar = view.findViewById(R.id.ratingBar);
        getData();
        setData();

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ImageActivity.class);
                intent.putExtra("image", dataImage);
                startActivity(intent);
            }
        });

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                mCallback.send(rating);
            }
        });

        return view;
    }

    private void getData() {
        DogBreeds dogBreeds=getArguments().getParcelable("dogBreeds");
        Log.d("DOGBREEDS", "dog breed: " + dogBreeds);
        dataName = dogBreeds.getName();
        dataDescription = dogBreeds.getDescription();
        dataImage = dogBreeds.getImages();
    }

    private void setData() {
        titleView.setText(dataName.toUpperCase());
        descriptionView.setText(dataDescription);
        imageView.setImageResource(dataImage);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Button btn = (Button) Objects.requireNonNull(getActivity()).findViewById(R.id.button);
        if(btn != null) {
            btn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    String url = "https://www.countryliving.com/life/kids-pets/g3283/the-50-most-popular-dog-breeds/";
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    try {
                        startActivity(intent);
                    } catch (ActivityNotFoundException ex) {
                        Toast.makeText(context, "Can't display web page", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
       inflater.inflate(R.menu.activity_menu, menu);
        for(int i = 0; i < menu.size(); i++){
            Drawable drawable = menu.getItem(i).getIcon();
            if(drawable != null) {
                drawable.mutate();
                drawable.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        DialogFragment shareDialog = new ShareDialog(getActivity(), userRating);
        shareDialog.show(getActivity().getSupportFragmentManager(), "Share");
        return super.onOptionsItemSelected(item);
    }
    public interface OnCallbackReceived {
        public void send(float userRating);
    }

}
