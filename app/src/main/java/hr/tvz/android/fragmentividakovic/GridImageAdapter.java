package hr.tvz.android.fragmentividakovic;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.ArrayList;
import java.util.List;

import hr.tvz.android.fragmentividakovic.DBFlow.DogBreedsDBModel;

public class GridImageAdapter extends BaseAdapter {
    private Context context;
    private List<Integer> images;
    private List<DogBreedsDBModel> listDBModel;

    public GridImageAdapter(Context c) {
        context = c;
        retrieveFromDatabase();
    }

    private void retrieveFromDatabase() {
        listDBModel= SQLite.select().from(DogBreedsDBModel.class).queryList();
        images = new ArrayList<>();

        for (DogBreedsDBModel dogModel: listDBModel) {
            images.add(dogModel.getImages());
        }
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public Object getItem(int position) {
        return images.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView = new ImageView(context);
        imageView.setLayoutParams(new GridView.LayoutParams(500, 580));
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setPadding(1,1,1,8);
        imageView.setImageResource(images.get(position));
        return imageView;
    }

}
